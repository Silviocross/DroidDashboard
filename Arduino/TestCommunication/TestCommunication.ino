//#include <SoftwareSerial.h>
//SoftwareSerial BT(10, 11); 
// creates a "virtual" serial port/UART
// connect BT module TX to D10
// connect BT module RX to D11
// connect BT Vcc to 5V, GND to GND

#define FASTCYCLE 200
#define SLOWCYCLE 1600

String string, string2;
int rpm = 0;
int spd = 0;
int gear = 3;
int ambientT = 18;
int motorT = 45;
int trip = 158;
int gas = 72;
unsigned long odo = 75000;
float battery = 13.2;
float lambda = 0.98;
unsigned long previousMillisFast = 0;
unsigned long previousMillisSlow = 0;

  void setup()
  {
    Serial.begin(9600);
 //   BT.begin(9600);
 //   Serial.println("Hello from Arduino");
  }

  void loop()
  {
    //  #rpm:spd:gear:lambda:
     
    string = "#";
    string += rpm;
    string += ":";
    string += spd;
    string += ":";
    string += gear;
    string += ":";
    string += lambda;
    string += "~";

    //  $battery:ambientT:motorT:trip:odo:gas:

    string2 = "$";
    string2 += battery;
    string2 += ":";
    string2 += ambientT;
    string2 += ":";
    string2 += motorT;
    string2 += ":";
    string2 += trip;
    string2 += ":";
    string2 += odo;
    string2 += ":";
    string2 += gas;
    string2 += "~";


  if(millis()-previousMillisSlow >= SLOWCYCLE){
      Serial.println(string2);
      previousMillisSlow = millis();
  }
  else if(millis()-previousMillisFast >= FASTCYCLE){
      Serial.println(string);
      previousMillisFast = millis();
  }
  
  //  Serial.println(string);
    delay(50);
    rpm = rpm + 100;
    spd = spd + 2;
    
    if(rpm >7500)
      rpm = 0;

    if(spd >140){
      spd = 0;
      gear = gear + 1;
      if(gear > 5)
        gear = 0;
    }

 }

 

    
