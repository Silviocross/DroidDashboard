int getgear(int previousGear) {

  // 1 - 842-854
  // N - 461-469
  // 2 - 505-514
  // 3 - 1002-1023
  // 4 - 461-470
  // 5 - 845-863

  int adc = analogRead(GEARPIN);

  if (adc < 470) {
    if (previousGear < 2)
      gearPosition = 0;
    else
      gearPosition = 4;
  }
  else if (adc < 600) {
    gearPosition = 2;
  }
  else if (adc < 950) {
    if (previousGear < 3)
      gearPosition = 1;
    else
      gearPosition = 5;
  }
  else {
    gearPosition = 3;
  }

  return gearPosition;
}
