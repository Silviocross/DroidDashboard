#include <EEPROM.h>

// Analog inputs
#define FUELPIN A0
#define GEARPIN A1
#define LAMBDAPIN A2
#define WATERPIN A3
#define VBATTPIN A4
#define TEMPPIN A5
#define AIN6 A6  // unused
#define AIN7 A7 // unused
// digital in-/outputs
#define SPEEDPIN 2
#define OKPIN 3 // INT 1 PIN
#define CANCELPIN 4   // LEFTPIN
#define RPMPIN 5
#define INPIN 6  // Empty
#define KEYPIN 7
#define PHONESUPPLYPIN 8
#define NEXTPIN 9
#define GRIPSPIN 10  // GRIPS
#define OUT1PIN 11
#define OUT2PIN 12  //
#define OUT3PIN 13  // HAZARD

#define FASTCYCLE 250
#define SLOWCYCLE 2005

#define SPEEDCHECKPERIOD 1000
#define WHEELPERIMETER 328  // 1970/6

#define BUTTONSDELAY 150

unsigned long previousButtonPress = 0;
unsigned long currentTime = 0;
unsigned long previousSpeedCheck = 0;
unsigned long pulse = 0;
long odometer= 0;

unsigned long previousMillisFast = 0;
unsigned long previousMillisSlow = 0;

boolean LCDstatus = 0;
int gripsHeating = 0;
int pulseCount = 0;
unsigned long tempRpm = 0;
int fuelLevelCount = 0;
int fuelLevelTemp = 0;

int oilError = 0;
int fuelLevel = 0;
int gearPosition = 0;
int rpm = 0;
int waterTemp = 0;
float batteryVoltage = 0;
int airTemp = 0;
long spd = 0;
int hazard = 0;
float lambda = 0;
int trip = 0;

int held = 0;
int countMeters = 0;


volatile long wheelRevolutions = 0;

String string, string2;
 

void setup(){
  
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
 
  //initializing pins
  pinMode(FUELPIN ,INPUT);
  pinMode(GEARPIN ,INPUT);
  pinMode(RPMPIN ,INPUT);
  pinMode(WATERPIN ,INPUT);
  pinMode(VBATTPIN ,INPUT);
  pinMode(TEMPPIN ,INPUT);
  pinMode(SPEEDPIN ,INPUT);
  pinMode(OKPIN ,INPUT);
  pinMode(CANCELPIN ,INPUT);
  pinMode(LAMBDAPIN ,INPUT);
  pinMode(KEYPIN ,INPUT);
  pinMode(PHONESUPPLYPIN ,OUTPUT);
  pinMode(NEXTPIN ,INPUT);
  pinMode(OUT2PIN ,OUTPUT);
  pinMode(OUT3PIN ,OUTPUT);

  
  attachInterrupt(0, getSpeedISR, RISING);  // ISR for getting speed

  // reading odometer from EEPROM

// setting EEPROM after flash
  if(EEPROMReadlong(1)<74000 || EEPROMReadlong(1) > 250000)
    EEPROMWritelong(1, 75000);

  //reading odo from EEPROM
   odometer = EEPROMReadlong(1); 
}


void loop(){

    // if there's any serial available, read it:
  while (Serial.available() > 0) {
    char c = Serial.read();  //gets one byte from serial buffer
   if(c == 'T'){
    trip = Serial.parseInt();
    }
    if(c == 'G'){
      gripsHeating = Serial.parseInt();
      }
  }
  
  // if key is switched ON, supply the phone
  if(digitalRead(KEYPIN) == HIGH){
    if(LCDstatus == 0){
      digitalWrite(PHONESUPPLYPIN, HIGH);
 //     odometer = readOdo();
      LCDstatus = 1;
    }
  }
  else{
    if(LCDstatus == 1){
      EEPROMWritelong(1, odometer);
      digitalWrite(PHONESUPPLYPIN, LOW);
      LCDstatus = 0;
    }
  }

/*
  // Reading cancel button for hazard  
while (digitalRead(CANCELPIN) == LOW){
   delay(100);
   held++;
   if (held < 12){ // the button was released in less than a second
   hazard = 0;
   }
   else{         // a second and a half has passed, note that the button may still be down
   hazard = 1;
     digitalWrite(OUT3PIN, hazard);  // hazard
  }
}
held = 0;
*/

  // gear position  
  gearPosition = getgear(gearPosition);

  // calculating motor speed
  pulse = pulseIn(RPMPIN,HIGH,16000);  
  if(pulseCount<4){
    if(pulse>0){
      tempRpm = tempRpm + ((unsigned long)60000000/(5*pulse));
      pulseCount = pulseCount + 1;
      }
    }
    else{
      pulseCount = 0;
      rpm = (int)tempRpm/5;
      tempRpm = 0;
      }    

   // calculating O2 value
  lambda = map(analogRead(LAMBDAPIN),0,1023,68,136);
  lambda = lambda/100;

  // Calculating fuel level   
  if(fuelLevelCount < 4){
    fuelLevelTemp = fuelLevelTemp + map(analogRead(FUELPIN),41,751,100,0);
    fuelLevelCount = fuelLevelCount + 1;
  }
  else{
    fuelLevel = fuelLevelTemp/5;
    fuelLevelTemp = 0;
    fuelLevelCount = 0;
    }

  // calculating battery voltage
  batteryVoltage = map(analogRead(VBATTPIN),0,1023,0,144);
  batteryVoltage = batteryVoltage/10;

  // calculating ait temperature
  airTemp = getTemperature();

  // calculating water temperature  
  //waterTemp = pulseIn(SPEEDPIN,LOW,40000);

  // calculating wheel speed
  spd = getSpeed(spd);
  
  // sending values  
  sendBluetooth();

  // setting outputs
  analogWrite(GRIPSPIN,map(gripsHeating,0,100,0,255));
  digitalWrite(OUT3PIN, hazard);  // hazard
}


void getSpeedISR(){  
  wheelRevolutions = wheelRevolutions+1;   

 }
