
 void sendBluetooth(){

  if(millis()-previousMillisSlow >= SLOWCYCLE){

    //  $battery:ambientT:motorT:trip:odo:gas:

    string2 = "$";
    string2 += batteryVoltage;
    string2 += ":";
    string2 += airTemp;
    string2 += ":";
    string2 += waterTemp;
    string2 += ":";
    string2 += trip;
    string2 += ":";
    string2 += odometer;
    string2 += ":";
    string2 += fuelLevel;
    string2 += "~";
    
      Serial.println(string2);
      previousMillisSlow = millis();
  }
  else if(millis()-previousMillisFast >= FASTCYCLE){

  //  #rpm:spd:gear:lambda:
     
    string = "#";
    string += rpm;
    string += ":";
    string += spd;
    string += ":";
    string += gearPosition;
    string += ":";
    string += lambda;
    string += "~";
    
      Serial.println(string);
      previousMillisFast = millis();
  }
    
}

