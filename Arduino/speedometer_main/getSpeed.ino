float getSpeed(int prevSpeed){
  if(millis()-previousSpeedCheck > SPEEDCHECKPERIOD){
    previousSpeedCheck = millis();

    // calculating speed
    unsigned long spdTemp = (wheelRevolutions*WHEELPERIMETER*36)/10000;
    
    // increasing trip and odo distance
    countMeters  = countMeters + (wheelRevolutions*WHEELPERIMETER)/1000;
    waterTemp = countMeters;
    if(countMeters>1000){
      trip = trip+1;
      odometer = odometer+1;
      countMeters = 0;
    }
      
    wheelRevolutions = 0;
    return spdTemp;
  }
  else{
    return prevSpeed;
  }
}
