int getTemperature(){
  
  int x = analogRead(TEMPPIN);
  
  int y = (-1.68672139469787E-9)*pow(x,4) + (3.95142674403642E-6)*pow(x,3) - (0.003342954246119)*pow(x,2) + (1.08329654532572)*x - 48;
//  int y = (-7.201E-7)*pow(x,3) + (0.000840)*pow(x,2) - (0.4181)*x + 71;
  return y;
  }
