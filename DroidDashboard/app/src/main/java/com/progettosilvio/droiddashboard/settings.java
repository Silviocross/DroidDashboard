package com.progettosilvio.droiddashboard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

public class settings extends AppCompatActivity {

    // defining widgets
    Button bluetoothSettingsButton;
    Button homeButton;
    Button launcherButton;
    Button shutOFFButton;
    SeekBar gripsSeekbar;

    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String gripsValue= "gripsKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //Calling widgets
        bluetoothSettingsButton = (Button) findViewById(R.id.btSettingsButton);
        homeButton = (Button) findViewById(R.id.homeButton);
        launcherButton = (Button) findViewById(R.id.launcherButton);
        shutOFFButton = (Button) findViewById(R.id.shutOFFButton);
        gripsSeekbar = (SeekBar) findViewById(R.id.gripsSeekBar);

        // reading saved grips heating value
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        gripsSeekbar.setProgress(sharedpreferences.getInt(gripsValue, 0));

        // defining listener for BT Settings pushbutton
        bluetoothSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                // Make an intent to start next activity.
                Intent i = new Intent(settings.this, deviceList.class);
                //Change the activity.
                startActivity(i);
            }
        });

        // defining listener for Home pushbutton
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(getApplicationContext(), "Back Home", Toast.LENGTH_SHORT).show();
                // Make an intent to start next activity.
                Intent i = new Intent(settings.this, MainActivity.class);
                //Change the activity.
                startActivity(i);
            }
        });

        // defining listener for Launcher pushbutton
        launcherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                // Make an intent to start next activity.
                Intent i = new Intent(settings.this, AppsListActivity.class);
                //Change the activity.
                startActivity(i);

            }
        });

        // defining listener for shutoff pushbutton
        shutOFFButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                try {
                    Process proc = Runtime.getRuntime()
                            .exec(new String[]{ "su", "-c", "reboot -p" });
                    proc.waitFor();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

        // defining listener for Grips seekbar
        gripsSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                // defining shared preferences
                SharedPreferences settings = getApplicationContext().getSharedPreferences(mypreference, 0);
                SharedPreferences.Editor editor = settings.edit();

                Toast.makeText(settings.this, "Set heated grips to: " + progressChangedValue + "%",
                        Toast.LENGTH_SHORT).show();
                editor.putInt(gripsValue, progressChangedValue);
                editor.apply();
            }
        });

    }
}
