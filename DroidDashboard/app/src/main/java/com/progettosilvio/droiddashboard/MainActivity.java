package com.progettosilvio.droiddashboard;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.UUID;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.os.PowerManager;
import android.location.LocationListener;



public class MainActivity extends AppCompatActivity implements LocationListener {

    // defining widgets
    Button naviButton, settingsButton;
    TextView txtString, txtStringLength, batteryVoltageText, speedText, gearText, lambdaText, ambientTempText, motorTempText, tripText, odoText;
    private Handler mHandler; // Our main handler that will receive callback notifications
    ProgressBar rpmBar, gasBar;


    StringBuilder fullMessage = new StringBuilder(200);

    // Bluetooth shit
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;

    private ConnectedThread mConnectedThread;

    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // #defines for identifying shared types between calling functions
    private final static int REQUEST_ENABLE_BT = 1; // used to identify adding bluetooth names
    private final static int MESSAGE_READ = 2; // used in bluetooth handler to identify message update
    private final static int CONNECTING_STATUS = 3; // used in bluetooth handler to identify message status

    // String for MAC address
    private static String address;

    // defining shared preferences
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String gripsValue= "gripsKey";

    // keep screen on
    protected PowerManager.WakeLock mWakeLock;
    Context context;
    int Brightness;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // hiding error icons
      //  oilImage.setVisibility(View.INVISIBLE);
      //  hazardImage.setVisibility(View.INVISIBLE);

        //Calling widgets
        rpmBar = (ProgressBar) findViewById(R.id.rpm_progress_bar);
        naviButton = (Button) findViewById(R.id.naviButton);
        settingsButton = (Button) findViewById(R.id.settingsButton);
        txtString = (TextView) findViewById(R.id.txtString);
        batteryVoltageText = (TextView) findViewById(R.id.batteryVoltageText);
        speedText = (TextView) findViewById(R.id.speedText);
        gearText = (TextView) findViewById(R.id.gearText);
        lambdaText = (TextView) findViewById(R.id.lambdaText);
        motorTempText = (TextView) findViewById(R.id.motorTempText);
        ambientTempText = (TextView) findViewById(R.id.ambientTempText);
        tripText = (TextView) findViewById(R.id.tripText);
        odoText = (TextView) findViewById(R.id.odoText);
        gasBar = (ProgressBar) findViewById(R.id.gas_progress_bar);

        // call the navigationBar (for fullscreen)
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);


        // brightness
        context = getApplicationContext();
        //Getting Current screen brightness.
        Brightness = Settings.System.getInt(context.getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,0);
     //   if(Settings.System.canWrite(context)){
     //       Settings.System.putInt(context.getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,1);
     //   }
     //   else {
     //       Toast.makeText(getApplicationContext(), "can't write brightness", Toast.LENGTH_LONG).show();
     //   }

        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        LocationManager lm = (LocationManager) this.getSystemService(context.LOCATION_SERVICE);
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);

        this.onLocationChanged(null);

        mHandler = new Handler(){
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void handleMessage(android.os.Message msg){
                if(msg.what == MESSAGE_READ){
                    String readMessage = null;
                    try {
                        readMessage = new String((byte[]) msg.obj, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    fullMessage.append(readMessage);                                      //keep appending to string until ~
                    int endOfLineIndex = fullMessage.indexOf("~");                    // determine the end-of-line
                    if (endOfLineIndex > 0) {                                           // make sure there data before ~
                        String dataInPrint = fullMessage.substring(0, endOfLineIndex);    // extract string

                        txtString.setText(dataInPrint);

                        String message = dataInPrint.substring(1);
                        if (readMessage.charAt(0) == '#') {                          //if it starts with # we know it is what we are looking for
                            //  #rpm:spd:gear:lambda:

                            // separate info from string
                            String[] separated = message.split(":");

                            // animating the rpm bar
                           // ObjectAnimator anim = ObjectAnimator.ofInt(rpmBar, "progress", rpmBar.getProgress(), Integer.parseInt(separated[0]));
                           // anim.setDuration(300);
                           // anim.setInterpolator(new DecelerateInterpolator());
                           // anim.start();

                           //    rpmBar.setProgress(Integer.parseInt(separated[0]),true);  // only latest android versions
                            rpmBar.setProgress(Integer.parseInt(separated[0]));

                            // showing speed
                        //    speedText.setText(separated[1]);

                            // showing gear
                            if (Integer.parseInt(separated[2]) == 0)   // writing neutral instead of 0
                                gearText.setText("N");
                            else
                                gearText.setText(separated[2]);

                            // showing lambda
                            lambdaText.setText(separated[3]);
                        } else if (readMessage.charAt(0) == '$') {
                            //  $battery:ambientT:motorT:trip:odo:gas:

                            String[] separated = message.split(":");
                            batteryVoltageText.setText(separated[0]);
                            ambientTempText.setText(separated[1]);
                            motorTempText.setText(separated[2]);
                            tripText.setText(separated[3]);
                            odoText.setText(separated[4]);
                            gasBar.setProgress(Integer.parseInt(separated[5]));
                        }

                        fullMessage.delete(0, fullMessage.length());                    //clear all string data
                        // strIncom =" ";
                        dataInPrint = " ";
                    }
                }

                if(msg.what == CONNECTING_STATUS){
                    if(msg.arg1 == 1)
                        txtStringLength.setText("Connected to Device: " + (String)(msg.obj));
                    else
                        txtStringLength.setText("Connection Failed");
                }
            }
        };

        btAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter
        checkBTState();

        /* This code together with the one in onDestroy()
         * will make the screen be always on until this Activity gets destroyed.
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        this.mWakeLock.acquire();
*/

        // defining listener for resetting Trip
        tripText.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                   Toast.makeText(getApplicationContext(), "Long click for resetting", Toast.LENGTH_LONG).show();
                // Fullscreen
                onWindowFocusChanged(true);
             }
        });

        tripText.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getApplicationContext(), "Trip Reset", Toast.LENGTH_LONG).show();
                mConnectedThread.write("T00");    // Send "TripReset" via Bluetooth
                // Fullscreen
                onWindowFocusChanged(true);
                return true;
            }
        });

        // defining listener for NAVI pushbutton
        naviButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Fullscreen
                onWindowFocusChanged(true);

                mConnectedThread.write("navi");    // Send "navi" via Bluetooth

                // launching navigation app
                Intent i = new Intent();
                PackageManager manager = getPackageManager();
               // i = manager.getLaunchIntentForPackage("com.google.android.apps.maps");
                i = manager.getLaunchIntentForPackage("com.here.app.maps");
                i.addCategory(Intent.CATEGORY_LAUNCHER);
                startActivity(i);
            }
        });


        // defining listener for SETTINGS pushbutton
        settingsButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Fullscreen
                onWindowFocusChanged(true);

                mConnectedThread.write("settings");    // Send "settings" via Bluetooth

                // Make an intent to start next activity.
                Intent i = new Intent(MainActivity.this, settings.class);
                //Change the activity.
                startActivity(i);

            }
        });

        // Fullscreen
        onWindowFocusChanged(true);
    }


    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        return  device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        //creates secure outgoing connecetion with BT device using UUID
    }

    /*
    @Override
    public void onDestroy() {
        this.mWakeLock.release();
        super.onDestroy();
    }
*/
    @Override
    public void onResume() {
        super.onResume();

        // Fullscreen
        onWindowFocusChanged(true);

        //Get MAC address from DeviceListActivity via intent
        Intent intent = getIntent();

        //Get the MAC address from the DeviceListActivty via EXTRA
      //  address = intent.getStringExtra(deviceList.EXTRA_ADDRESS);
          address = "98:D3:32:71:1A:8D";
        //   address = newint.getStringExtra(deviceList.EXTRA_ADDRESS); //receive the address of the bluetooth device

        //create device and set the MAC address
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_LONG).show();
        }
        // Establish the Bluetooth socket connection.
        try
        {
            btSocket.connect();
        } catch (IOException e) {
            try
            {
                btSocket.close();
            } catch (IOException e2)
            {
                //insert code to deal with this
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();

        //I send a character when resuming.beginning transmission to check device is connected
        //If it is not an exception will be thrown in the write method and finish() will be called
        mConnectedThread.write("x");

        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        mConnectedThread.write("G" + sharedpreferences.getInt(gripsValue, 0));    // Send "settings" via Bluetooth4
    }

    @Override
    public void onPause()
    {
        super.onPause();
        try
        {
            //Don't leave Bluetooth sockets open when leaving activity
            btSocket.close();
        } catch (IOException e2) {
            //insert code to deal with this
        }
    }

    // Running the app fullscreen
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    //Checks that the Android device Bluetooth is available and prompts to be turned on if off
    private void checkBTState() {

        if(btAdapter==null) {
            Toast.makeText(getBaseContext(), "Device does not support bluetooth", Toast.LENGTH_LONG).show();
        } else {

            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location==null){
            speedText.setText("-");
        }
        else{
        //    float nCurrentSpeed = location.getSpeed();
            speedText.setText(speedKMH(location.getSpeed()));
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private String speedKMH(float speed)
    {
        //here speed is the value extracted from speed=location.getSpeed();
        //for km/hour
        double a = 3.6 * speed;
        int kmhSpeed = (int) a/1;

        return Integer.toString(kmhSpeed);
    }
    //create new class for connect thread
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()
            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.available();
                    if(bytes != 0) {
                        buffer = new byte[1024];
                        SystemClock.sleep(200); //pause and wait for rest of data. Adjust this depending on your sending speed.
                        bytes = mmInStream.available(); // how many bytes are ready to be read?
                        bytes = mmInStream.read(buffer, 0, bytes); // record how many bytes we actually read
                        mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                                .sendToTarget(); // Send the obtained bytes to the UI activity
                    }
                } catch (IOException e) {
                    e.printStackTrace();

                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String input) {
            byte[] bytes = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                // Make an intent to start next activity.
                Intent i = new Intent(MainActivity.this, deviceList.class);
                startActivity(i);
            }
        }
    }

}
